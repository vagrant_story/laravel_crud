<?php
namespace App\Imports;

use App\Product;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithChunkReading;

ini_set('max_execution_time', 900);//nginx поставить так же

class ProductsImport implements ToModel, WithCustomCsvSettings, WithChunkReading
{
    use Importable;

    /**
     * @var string
     */
    public static $delimiter_custom = "";

    /**
    * @param string $delimiter_custom
    */
    public static function setDelimiterCustom(string $delimiter_custom): void
    {
        self::$delimiter_custom = $delimiter_custom;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Product([
            'product_code'  => $row['product_code'],
            'product_name' => $row['product_name'],
            'product_description' => $row['product_description'],
            'stock' => $row['stock'],
            'cost_in_gbp' => $row['cost_in_gbp'],
            'discontinued' => $row['discontinued'],
        ]);
    }

    /**
     * @return int
     */
    public function headingRow(): int
    {
        return 1;
    }

    /**
     * @return array
     */
    public function getCsvSettings(): array
    {
        return [
            'delimiter' => self::$delimiter_custom
        ];
    }

    /**
     * @return int
     */
    public function batchSize(): int
    {
        return 1000;
    }

    /**
     * @return int
     */
    public function chunkSize(): int
    {
        return 1000;
    }
}
