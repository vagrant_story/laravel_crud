<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    const STATUS_SUCCESS =  'success';
    const STATUS_ERROR =  'error';

    protected $fillable = [
        'batch_id',
        'status',
        'message'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function batch()
    {
        return $this->belongsTo('App\Batch');
    }

    /**
     * @return mixed
     */
    public function getMessageAttribute()
    {
        return json_decode($this->attributes['message'], true);
    }
}
