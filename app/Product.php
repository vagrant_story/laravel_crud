<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    /**
     * Message success method
     *
     * @var string
     */
    public $successReturn = '';

    protected $fillable = [
        'code',
        'name',
        'description',
        'stock',
        'cost',
        'discontinued',
        'published_at',
        'user_id',
        'published'
    ];
    public $dates = ['published_at'];

    /**
     * @param $query
     */
    public function scopePublishedAt($query)
    {
        $query->where('published_at', '<=', Carbon::now());
    }

    /**
     * @param $query
     */
    public function scopePublished($query)
    {
        $query->where('published', '=', '1');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function batch()
    {
        return $this->belongsTo('App\Batch');
    }

    /**
     * @param $data
     */
    public function setDiscontinuedAttribute($data)
    {
        $discontinued = '';

        if ($data==='yes')
        {
            $discontinued = Carbon::now()->format('Y-m-d');
        }
        else
        {
            $discontinued =  null;
        }
        $this->attributes['discontinued']=$discontinued;
    }

    public function setCodeAttribute($data)
    {
        $this->attributes['code']=(string)$data;
    }

    /**
     * Set message success method
     *
     * @param string $success
     */
    public function successReturn(string $success)
    {
        $this->successReturn = $success;
    }

}