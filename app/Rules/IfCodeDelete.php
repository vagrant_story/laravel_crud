<?php

namespace App\Rules;

use App\Product;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class IfCodeDelete implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $query = Product::where($attribute,$value)->withTrashed();
        $query_were = $query->count();
        if ($query_were)
        {
            if (Auth::user()->isAdmin())
            {
                return true;
            }
            else
            {
                return $query->where('user_id', Auth::id())->count();
            }
        }
        else
        {
            return true;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'BELONG TO ANOTHER USER!!!';
    }
}
