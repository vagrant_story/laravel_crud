<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UploadProductsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'csv' => 'required|mimes:csv,txt,xlsx', //max file size, reg возможные символы
//            'csv' => 'required|mimes:csv,txt, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
//            'csv' => 'required|mimetypes:application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        ];
    }
}
