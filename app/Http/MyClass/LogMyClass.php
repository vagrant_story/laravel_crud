<?php

namespace App\Http\MyClass;

use App\Batch;
use App\Log;
use App\Product;

class LogMyClass
{
    /**
     * Add errors in to logs table in base
     *
     * @param $validator
     * @param $row
     * @param $key
     */
    public function setErrors($validator, $row, $key)
    {
        $validator
            ->errors()
            ->add('code', $row['code'])
            ->add('row', ($key + 1 + ImportMyClass::ROW_OFFSET));
        $error_products = $validator->messages()->toJson();
        $log = new Log();
        $log->batch_id = ImportMyClass::getBatch()->id;
        $log->product_id = Product::where('code',$row['code'])->value('id');
        $log->message = $error_products;
        $log->status = Log::STATUS_ERROR;
        $log->save();
    }

    /**
     * Add success in to logs table in base
     *
     * @param $prod
     * @param $key
     */
    public function setSuccess($prod, $key)
    {
        if ($prod->successReturn)
        {
            $log = new Log();
            $log->batch_id =  ImportMyClass::getBatch()->id;
            $log->product_id = Product::where('code',$prod->code)->value('id');
            $message = ['success'=>"$prod->successReturn product with code $prod->code row " . ($key + 1 + ImportMyClass::ROW_OFFSET)];
            $log->message = json_encode($message);
            $log->status = Log::STATUS_SUCCESS;
            $log->save();
        }
    }

    /**
     * Get errors from logs table
     *
     * @return mixed
     */
    public function getErrors(Batch $batch)
    {
        return $this->getLogs($batch, 'error');
    }

    /**
     * Get success from logs table
     *
     * @return mixed
     */
    public function getSuccess(Batch $batch)
    {
        return $this->getLogs($batch, 'success');
    }

    /**
     * get Logs switch status by $status
     *
     * @param Batch $batch
     * @param string $status
     * @return mixed
     */
    public function getLogs(Batch $batch, string $status)
    {
        return Log::where('batch_id', $batch->id)
            ->where('status', $status)
            ->get()
            ->pluck('message')
            ->toArray();
    }

    /**
     * Add custom error in logs table
     *
     * @param string $error
     */
    public function setErrorCustom(string $error)
    {
        $full['Custom'][] = $error;
        $log = new Log();
        $log->batch_id = ImportMyClass::getBatch()->id;
        $log->message = json_encode($full);
        $log->status = Log::STATUS_ERROR;
        $log->save();
    }
}
