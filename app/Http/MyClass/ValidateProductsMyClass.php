<?php

namespace App\Http\MyClass;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Pagination;
use Illuminate\Support\Collection;

class ValidateProductsMyClass extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rulesMyClass($data)
    {
        $validator = Validator::make($data, [
            'code' =>'required',
            'name' =>'required',
            'description' =>'required',
            'stock' =>'required|numeric',
            'cost' => 'required|max:300|numeric',
        ]);
        $validator->sometimes('cost', 'required|min:5', function ($input) {
            return $input->stock < 10;
        });
        return $validator;
    }
//    public function paginate($items, $perPage = 15, $page = null, $options = [])
//    {
//        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
//        $items = $items instanceof Collection ? $items : Collection::make($items);
//        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
//    }

}
