<?php

namespace App\Http\MyClass\Validator;

use App\Http\MyInterface\RowValidatorInterface;
use App\Rules\IfCodeDelete;
use App\Rules\IfExist;
use App\Rules\IfExistNoTrash;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Pagination;

class DeleteValidate extends FormRequest implements RowValidatorInterface
{
    /**
     * Validate row
     *
     * @param $row
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validate(array $row)
    {
        $validator = Validator::make($row, [
            'code' =>[
                'required',
                'regex:/^[A-Za-z0-9]+$/i',
                new IfExistNoTrash(),
                new IfExist(),
                new IfCodeDelete(),
            ],
        ]);
        return $validator;
    }
}