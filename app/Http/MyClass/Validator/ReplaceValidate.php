<?php

namespace App\Http\MyClass\Validator;

use App\Http\MyInterface\RowValidatorInterface;
use App\Rules\IfCodeDelete;
use App\Rules\IfExist;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class ReplaceValidate extends FormRequest implements RowValidatorInterface
{
    /**
     * Validate row
     *
     * @param $row
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validate(array $row)
    {
        $validator = Validator::make($row, [
            'code' =>[
                'required',
                'regex:/^[A-Za-z0-9]+$/i',
                new IfExist(),
                new IfCodeDelete(),
            ],
            'name' =>'required|regex:/^[A-Za-z0-9-.\s]+$/i',
            'description' =>'required|regex:/^[A-Za-z0-9-.\s!\']+$/i',
            'stock' =>'required|numeric',
            'cost' => 'required|max:300|numeric',
        ]);
        $validator->sometimes('cost', 'required|min:5', function ($input) {
            return $input->stock < 10;
        });
        return $validator;
    }
}