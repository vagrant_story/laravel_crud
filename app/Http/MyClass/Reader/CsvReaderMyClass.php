<?php

namespace App\Http\MyClass\Reader;

use App\Http\MyClass\ImportMyClass;
use App\Http\MyInterface\ReaderInterface;
use App\Imports\ProductsImport;
use Illuminate\Http\UploadedFile;

class CsvReaderMyClass implements ReaderInterface
{
    /**
     * Get array with all row
     *
     * @param UploadedFile $file
     * @return array
     */
    public function reader(UploadedFile $file):array
    {
        $headers = [
            'code',
            'name',
            'description',
            'stock',
            'cost',
            'discontinued',
        ];
        $array = (new ProductsImport)->toArray($file);
        $data = array();
        for ($i = 0; $i < ImportMyClass::ROW_OFFSET; $i++)
        {
            unset($array[0][$i]);
            $i++;
        }
        foreach ($array[0] as $index => $row)
        {
            if (count($row) < count($headers))
            {
                continue;
            }
            $data[] = array_combine($headers, $row);
        }
        return $data;
    }
}
