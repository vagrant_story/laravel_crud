<?php

namespace App\Http\MyClass;

use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Pagination;
use App\Http\Controllers\Auth\RegisterController;

class ValidateUsersMyClass extends RegisterController
{
    protected function validatorUpdate(array $data, $user_id)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', "unique:users,email, $user_id"],
            'password' => ['sometimes','required', 'string', 'min:8', 'confirmed'],
        ]);
    }
    public function validatorExecute(array $data)
    {
        return $this->validator($data);
    }
    public function createExecute(array $data)
    {
        return $this->create($data);
    }

    public function updateExecute(array $data, $user_id)
    {
        return $this->validatorUpdate($data, $user_id);
    }
}
