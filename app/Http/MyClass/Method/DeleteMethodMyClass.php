<?php

namespace App\Http\MyClass\Method;

use App\Batch;
use App\Http\MyClass\LogMyClass;
use App\Http\MyInterface\StrategyInterface;
use App\Product;
use Illuminate\Support\Facades\Auth;

class DeleteMethodMyClass implements StrategyInterface
{
    /**
     * Execute method
     *
     * @param array $row
     * @param Batch $batch
     * @return Product
     */
    public function processRow(array $row, Batch $batch):Product
    {
        $product = new Product($row);
        $product->batch_id = $batch->id;
        Product::where('code',$product->code)->delete();
        $product->successReturn('DELETED');
        return $product;
    }
}