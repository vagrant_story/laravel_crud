<?php

namespace App\Http\MyClass\Method;

use App\Batch;
use App\Http\MyInterface\StrategyInterface;
use App\Product;
use Illuminate\Support\Facades\Auth;

class InsertUpdateMethodMyClass implements StrategyInterface
{
    /**
     * Execute method
     *
     * @param array $row
     * @param Batch $batch
     * @return Product
     */
    public function processRow(array $row, Batch $batch):Product
    {
        $product = new Product($row);
        $code_row = Product::where('code',$product->code);
        $query_were = Product::where('code',$product->code)->withTrashed();
        $query_trash = Product::where('code',$product->code)->onlyTrashed();
        $product->batch_id = $batch->id;
        if (!$query_were->count())
        {
            Auth::user()->products()->save($product);
            $product->successReturn('INSERT');
            return $product;
        }
        else
        {
            $query_trash->count() ? $query_trash->restore() : '';
            $cost = $code_row->value('cost')+$product->cost;
            $stock = $code_row->value('stock')+$product->stock;
            ($cost < 0) ? $cost = 0 : $cost;
            ($stock < 0) ? $stock = 0 : $stock;
            $code_row->update(['cost' => $cost, 'stock' => $stock]);
            $product->successReturn('UPDATE');
            return $product;
        }
    }
}