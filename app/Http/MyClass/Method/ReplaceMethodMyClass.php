<?php

namespace App\Http\MyClass\Method;

use App\Batch;
use App\Http\MyInterface\StrategyInterface;
use App\Product;
use Illuminate\Support\Facades\Auth;

class ReplaceMethodMyClass implements StrategyInterface
{
    /**
     * Execute method
     *
     * @param array $row
     * @param Batch $batch
     * @return Product
     */
    public function processRow(array $row, Batch $batch):Product
    {
        $product = new Product($row);
        $product->batch_id = $batch->id;
        $code_row = Product::where('code',$product->code);
        $query_trash = Product::where('code',$product->code)->onlyTrashed();
        $query_trash->count() ? $query_trash->restore() : '';
        $code_row->update($product->toArray());
        $product->successReturn('REPLACE');
        return $product;
    }
}