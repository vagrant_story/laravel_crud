<?php

namespace App\Http\MyClass;

use App\Http\MyClass\Method\DeleteMethodMyClass;
use App\Http\MyClass\Method\InsertUpdateMethodMyClass;
use App\Http\MyClass\Method\ReplaceMethodMyClass;

class StrategyMyClass
{
    /**
     * Get method object
     *
     * @param $method
     * @return DeleteMethodMyClass|InsertUpdateMethodMyClass|ReplaceMethodMyClass
     */
    public function strategy(string $method):object
    {
        switch ($method)
        {
            case 'INSERT':
                $method = new InsertUpdateMethodMyClass();
                break;
            case 'REPLACE':
                $method = new ReplaceMethodMyClass();
                break;
            case 'DELETE':
                $method = new DeleteMethodMyClass();
                break;
            default:
                $log = new LogMyClass();
                $log->setErrorCustom('Method is not detected (Input,replace,delete)');
        }
        return $method;
    }
}
