<?php

namespace App\Http\MyClass;

use App\Batch;
use App\Http\MyClass\Reader\CsvReaderMyClass;
use App\Http\MyClass\Reader\XlsxReaderMyClass;
use App\Http\MyClass\Validator\DeleteValidate;
use App\Http\MyClass\Validator\InsertUpdateValidate;
use App\Http\MyClass\Validator\ReplaceValidate;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;


/**
 * Class ImportMyClass
 *
 * @package App\Http\MyClass
 */
class ImportMyClass
{
    /**
     * Const offset
     */
    const ROW_OFFSET = '1';

    /**
     * batch import object
     *
     * @var
     */
    protected static $batch;

    /**
     * Get batch object
     *
     * @return mixed
     */
    public static function getBatch()
    {
       return self::$batch;
    }

    /**
     * Import function return a batch object
     *
     * @param UploadedFile $file
     * @param string $type
     * @param string $method
     * @return object
     */
    public function import(UploadedFile $file, string $type, string $method):object
    {
        $batch = $this->createBatch(); //get Batch
        $validators = $this->getValidator($method);
        $reader = $this->getReader($type); // return object reader
        $rows =  $reader->reader($file); // return $array
        $strategy = $this->getStrategy($method); // object strategy
        if (!$rows)
        {
            $log = new LogMyClass();
            $log->setErrorCustom('File is EMPTY or Try another Delimiter');
        }
        foreach ($rows as $key => $row)
        {
            $validator = $validators->validate($row);
            if ($validator->fails())
            {
                $log = new LogMyClass();
                $log->setErrors($validator, $row, $key);
            }
            else
            {
                $prod = $strategy->processRow($row, $batch);
                $log = new LogMyClass();
                $log->setSuccess($prod, $key);
            }
        }
        return $batch;
    }

    /**
     * Get reader object
     *
     * @param string $type
     * @return object
     */
    public function getReader(string $type):object
    {
        switch ($type)
        {
            case 'csv':
                $reader = new CsvReaderMyClass();
                break;
            case 'xlsx':
                $reader = new XlsxReaderMyClass();
                break;
            default:
                $log = new LogMyClass();
                $log->setErrorCustom('Type is not detected (csv, xlsx nedeed)');
        }
        return $reader;
    }

    /**
     * Get validator object
     *
     * @param string $method
     * @return object
     */
    public function getValidator(string $method):object
    {
        switch ($method)
        {
            case 'INSERT':
                $validator = new InsertUpdateValidate();
                break;
            case 'REPLACE':
                $validator = new ReplaceValidate();
                break;
            case 'DELETE':
                $validator = new DeleteValidate();
                break;
            default:
                $log = new LogMyClass();
                $log->setErrorCustom('Validate is not detected (INSERT, REPLACE, DELETE nedeed)');
        }
        return $validator;
    }

    /**
     * Get method object
     *
     * @param string $method
     * @return object
     */
    public function getStrategy(string $method):object
    {
        $strategy = new StrategyMyClass();
        $strategy = $strategy->strategy($method);
        return $strategy;
    }

    /**
     * Create batch
     *
     * @return Batch
     */
    public function createBatch():Batch
    {
        $batch = Auth::user()->batch()->save(new Batch());
        self::$batch = $batch;
        return $batch;
    }
}