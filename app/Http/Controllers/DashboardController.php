<?php

namespace App\Http\Controllers;

use App\Http\MyClass\Validator\InsertUpdateValidate;
use App\Http\MyClass\Validator\ReplaceValidate;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\MyClass\ValidateUsersMyClass;
use Illuminate\Support\Facades\Hash;

class DashboardController extends Controller
{
    /**
     * Show all products
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        $products = Product::latest('published_at')->paginate(15);
        return view('admin.index')->with('products', $products);
    }

    /**
     * Show all users
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showUsers()
    {
        $users = User::paginate(15);
        return view('admin.users')->with('users', $users);
    }

    /**
     * Delete product
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteProduct(Request $request)
    {
        Product::FindOrFail($request->id)->delete();
        return redirect('admin');
    }

    /**
     * Update product
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateProduct(Request $request)
    {
        $validator = (new ReplaceValidate())->validate($request->toArray());
        if ($validator->fails())
        {
            return redirect('admin')
                ->withErrors($validator)->with('status', $request->code);
        }
        else
        {
            Product::FindOrFail($request->id)->update($request->all());
            return redirect('admin');
        }
    }

    /**
     * Add product
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function addProduct(Request $request)
    {
        $validator = (new InsertUpdateValidate())->validate($request->toArray());
        if ($validator->fails())
        {
            return redirect('admin')
                ->withErrors($validator);
        }
        else
        {
            $request = new Product($request->all());
            Auth::user()->products()->save($request);
            return redirect('admin');
        }
    }

    /**
     * Update user
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateUser(Request $request)
    {
        $user = new ValidateUsersMyClass;
        $validator = $user->updateExecute($request->all(), $request->id);
        if ($validator->fails()) {
            return redirect('admin/users')
                ->withErrors($validator)->with('status', $request->id);
        }
        $request->merge([
            'password' => Hash::make($request->password),
        ]);
        User::FindOrFail($request->id)->update($request->all());
        return redirect('admin/users');
    }

    /**
     * Add user
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function addUser(Request $request)
    {
        $user = new ValidateUsersMyClass;
        $validator = $user->validatorExecute($request->all());
        if ($validator->fails()) {
            return redirect('admin/users')
                ->withErrors($validator);
        }
        $user->createExecute($request->all());
        return redirect('admin/users');
    }

    /**
     * Delete user
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteUser(Request $request)
    {
        if (User::find($request->id)->isAdmin())
        {
            return redirect('admin/users')->with('permission', 'Do not delete user with permission Admin');
        }
        else
        {
            User::FindOrFail($request->id)->delete();
            return redirect('admin/users');
        }

    }
}
