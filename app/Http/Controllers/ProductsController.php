<?php

namespace App\Http\Controllers;

use App\Http\MyClass\LogMyClass;
use App\Http\Requests\UploadProductsRequest;
use App\Imports\ProductsImport;
use App\Product;
use App\Http\MyClass\ImportMyClass;
use Illuminate\Support\Facades\Auth;

class ProductsController extends Controller
{
    /**
     * Authorised for upload page
     *
     * ProductsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth', ['only' => 'upload']);
    }

    /**
     * List of products
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $products = Product::latest('published_at')->published()->publishedAt()->paginate(15);
        return view('products.index')->with('products', $products);
    }

    /**
     * List of products only Auth::user()
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function indexUser()
    {
        $products = Product::latest('published_at')->where('user_id', Auth::id())->published()->publishedAt()->paginate(15);
        return view('products.index')->with('products', $products);
    }

    /**
     * Show products upload page
     *
     * @return view('products.upload')
     */
    public function upload()
    {
        return view('products.upload');
    }

    /**
     * Add uploaded file into database
     *
     * @param UploadProductsRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function store(UploadProductsRequest $request)
    {
        ProductsImport::setDelimiterCustom($request->delimiter);
        $file = $request->file('csv');
        $extension = $file->getClientOriginalExtension();
        $method = $request->upload_method;
        $batch = (new ImportMyClass)->import($file, $extension, $method); //batch получить вместо лог объекта и потом получить по батчу лог
        $log = new LogMyClass();
        if ($log->getSuccess($batch) || $log->getErrors($batch))
        {
            return view('errors/index', ['success_message_array' => $log->getSuccess($batch), 'error_products_array' => $log->getErrors($batch)]);
        }
        else
        {
            return redirect('/');
        }
    }
}
