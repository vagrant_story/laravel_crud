<?php

namespace App\Http\MyInterface;

use Illuminate\Http\UploadedFile;

interface ReaderInterface
{
    /**
     * Get array with all row
     *
     * @param UploadedFile $file
     * @return mixed
     */
    public function reader(UploadedFile $file);
}
