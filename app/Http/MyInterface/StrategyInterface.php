<?php

namespace App\Http\MyInterface;

use App\Batch;

interface StrategyInterface
{
    /**
     * Execute method
     *
     * @param $row
     * @param $batch
     * @return mixed
     */
    public function processRow( array $row, Batch $batch);

}
