<?php

namespace App\Http\MyInterface;


interface RowValidatorInterface
{
    /**
     * Validate row
     *
     * @param array $row
     * @return mixed
     */
    public function validate(array $row);
}