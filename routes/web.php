<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {    return view('products/index');});

Route::get('/', 'ProductsController@index');
Route::get('/userproducts', 'ProductsController@indexUser');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('products/upload', 'ProductsController@upload');
//Route::post('/', 'ProductsController@storeMethod');
Route::post('/', 'ProductsController@store');



// Admin routes
Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function() {

    Route::get('/', 'DashboardController@show');
    Route::post('/', 'DashboardController@deleteProduct');
    Route::patch('/', 'DashboardController@updateProduct');
    Route::put('/', 'DashboardController@addProduct');

    Route::get('users', 'DashboardController@showUsers');
    Route::post('users', 'DashboardController@deleteUser');
    Route::patch('users', 'DashboardController@updateUser');
    Route::put('users', 'DashboardController@addUser');
});
