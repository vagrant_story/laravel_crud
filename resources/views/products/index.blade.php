@extends('welcome')

@section('content')
<div class="container-fluid">
    {{--@if(Session::has('error_products_array'))--}}
    {{--    <div class="alert alert-success">--}}
    {{--        {{session()->get('error_products_array')}}--}}
    {{--    </div>--}}
{{--    @if($error_products_array)--}}
{{--        @foreach($error_products_array as $error)--}}
{{--            <tr>--}}

{{--                <td>{{ $error[1][0] }}</td>--}}

{{--            </tr>--}}
{{--        @endforeach--}}
{{--    @endif--}}
{{--    --}}{{--@endif--}}
    <div class="row">
        <div class="col">
            <table class="table table-sm">
                <thead>
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Code</th>
                    <th scope="col">Description</th>
                    <th scope="col">Stock</th>
                    <th scope="col">Cost in GBP</th>
                    <th scope="col">Discontinued</th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                    <tr>
                        <th scope="row">{{ $product->name }}</th>
                        <td>{{ $product->code }}</td>
                        <td>{{ $product->description }}</td>
                        <td>{{ $product->stock }}</td>
                        <td>{{ $product->cost }}</td>
                        <td>{{ $product->discontinued }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $products->links() }}
        </div>
    </div>
</div>
@stop