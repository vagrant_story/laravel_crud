@extends('welcome')

@section('content')
{!! Form::open(['url' => '/', 'files'=>'true', 'class'=> 'md-form']) !!}
<div class="file-field">
    {!!   Form::submit('Upload', ['class'=> 'btn btn-primary btn-sm float-left']) !!}
    {!!   Form::label('upload', 'Upload your file with products:') !!}
    {!!   Form::file('csv', ['class'=> 'btn btn-primary btn-sm']) !!}
    {!!   Form::label('method', 'method download:') !!}
    {!! Form::select('upload_method', array('INSERT' => 'Insert', 'REPLACE' => 'Replace', 'DELETE' => 'Delete'), 'IN'); !!}
    {!!   Form::label('delimiter', 'delimiter csv:') !!}
    {!! Form::select('delimiter', array("," => ',', ';' => ";", '|' => "|"), 'IN'); !!}

</div>
{!! Form::close() !!}
@stop