@extends('welcome')

@section('content')
    <div class="container-fluid">

{{--        @if(Session::has('error_products_array'))--}}
{{--            <div class="alert alert-success">--}}
{{--                {{session()->get('error_products_array')}}--}}
{{--            </div>--}}
            @if($error_products_array)
            <h2>Errors</h2>
                 @foreach($error_products_array as $error)
                     <tr>
                         @foreach($error as $key=>$err)
                             @foreach($err as $er)
                                <td>{{ $key }}: {{ $er }} /</td>
                             @endforeach
                         @endforeach
                     </tr>
                     <hr/>
                @endforeach
            @endif
            @if($success_message_array)
                <h2>Success</h2>
                   <tr>
                @foreach($success_message_array as $message)
                           @foreach($message as $mess)
                           <td>{{ $mess }}</td>
                           @endforeach
                       <hr/>
                @endforeach
                    </tr>
            @endif
{{--            @endif--}}

    </div>
@stop