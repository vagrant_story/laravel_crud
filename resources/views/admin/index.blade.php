@extends('welcome')

@section('content')
    @if (session('status'))
        <script>
            $( document ).ready(function() {
                $( ".code:contains('{{ session('status') }}')" ).parent().css( {"border": "3px solid #ff0000", "border-radius":"0.25rem"} );
            });
        </script>
{{--        <div class="alert alert-success">--}}
{{--            {{ session('status') }}--}}
{{--        </div>--}}
    @endif
    <div class="container-fluid">
        <h2>Dashboard</h2>
        <div class="row">
            <div class="col">
                <table class="table table-sm">
                    <thead>
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">User_id</th>
                        <th scope="col">Name</th>
                        <th scope="col">Code</th>
                        <th scope="col">Description</th>
                        <th scope="col">Stock</th>
                        <th scope="col">Cost in GBP</th>
                        <th scope="col">Published_date</th>
                        <th scope="col">Published</th>
                        <th scope="col">Discontinued_date</th>
                        <th scope="col">Discontinued</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="form-group{{ $errors->any() ? ' has-error' : '' }}">
                        <td>id</td>
                        <td>user_id</td>
                        {!! Form::open(['method' => 'PUT', 'action' => ['DashboardController@addProduct']]) !!}
                        <td>{!!  Form::text('name', '', ['class'=> 'form-control']) !!}</td>
                        <td>{!!  Form::text('code', '', ['class'=> 'form-control']) !!}</td>
                        <td>{!!  Form::textarea('description', '', ['class'=> 'form-control anim-css']) !!}</td>
                        <td>{!!  Form::number('stock', '', ['class'=> 'form-control']) !!}</td>
                        <td>{!!  Form::number('cost', '', ['class'=> 'form-control','step' => '0.01']) !!}</td>
                        <td>published_at</td>
                        {!! Form::hidden('published','0') !!}
                        <td>{!!  Form::checkbox('published', '1', true, ['class'=> 'form-control']) !!}</td>
                        <td>discontinued data</td>
                        {!! Form::hidden('discontinued','') !!}
                        <td>{!!  Form::checkbox('discontinued', 'yes', false, ['class'=> 'form-control']) !!}</td>
                        <td>

                            {!!  Form::submit('Add', ['class'=> 'btn btn-primary form-control']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                    @foreach($products as $product)
                        <tr class="form-group{{ $errors->any() ? ' has-error' : '' }}">
                            <td>{{ $product->id }}</td>
                            <td>{{ $product->user_id }}</td>
                            {!! Form::open(['method' => 'PATCH', 'action' => ['DashboardController@updateProduct']]) !!}
                            <td>{!!  Form::text('name', $product->name, ['class'=> 'form-control']) !!}</td>
                            <td class="code">{{ $product->code }}</td>
                            {!! Form::hidden('code',$product->code) !!}
                            <td>{!!  Form::textarea('description', $product->description, ['class'=> 'form-control anim-css']) !!}</td>
                            <td>{!!  Form::number('stock', $product->stock, ['class'=> 'form-control']) !!}</td>
                            <td>{!!  Form::number('cost', $product->cost, ['class'=> 'form-control','step' => '0.01']) !!}</td>
                            <td>{{ $product->published_at }}</td>
                            {!! Form::hidden('published','0') !!}
                            <td>{!!  Form::checkbox('published', '1', $product->published, ['class'=> 'form-control']) !!}</td>
                            <td>{{ $product->discontinued }}</td>
                            {!! Form::hidden('discontinued','') !!}
                            <td>{!!  Form::checkbox('discontinued', 'yes', false, ['class'=> 'form-control']) !!}</td>
                            <td>
                                {!! Form::hidden('id',$product->id) !!}
                                {!!  Form::submit('Update', ['class'=> 'btn btn-primary form-control']) !!}
                                {!! Form::close() !!}
                            </td>
                            <td>
                            {!! Form::open(['url' => 'admin']) !!}
                                {!! Form::hidden('id',$product->id) !!}
                                {!!  Form::submit('Delete', ['class'=> 'btn btn-primary form-control']) !!}
                            {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $products->links() }}
            </div>
        </div>
    </div>
@stop