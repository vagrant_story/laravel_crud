@extends('welcome')

@section('content')
    <div class="container-fluid">
        {{--@if(Session::has('error_products_array'))--}}
        {{--    <div class="alert alert-success">--}}
        {{--        {{session()->get('error_products_array')}}--}}
        {{--    </div>--}}
        {{--    @if($error_products_array)--}}
        {{--        @foreach($error_products_array as $error)--}}
        {{--            <tr>--}}

        {{--                <td>{{ $error[1][0] }}</td>--}}

        {{--            </tr>--}}
        {{--        @endforeach--}}
        {{--    @endif--}}
        {{--    --}}{{--@endif--}}
        <h2>Dashboard Edit Product</h2>
        <div class="row">
            <div class="col">
                <table class="table table-sm">
                    <thead>
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">Name</th>
                        <th scope="col">Code</th>
                        <th scope="col">Description</th>
                        <th scope="col">Stock</th>
                        <th scope="col">Cost in GBP</th>
                        <th scope="col">Discontinued</th>
                        <th scope="col">Update</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>{{ $product->id }}</td>
                            <th scope="row">{{ $product->name }}</th>
                            {!! Form::open(['url' => 'admin']) !!}
                            <td>{!!  Form::text('code', $product->code, ['class'=> 'form-control']) !!}</td>
                            <td>{!!  Form::text('description', $product->description, ['class'=> 'form-control']) !!}</td>
                            <td>{!!  Form::text('stock', $product->stock, ['class'=> 'form-control']) !!}</td>
                            <td>{!!  Form::text('cost', $product->cost, ['class'=> 'form-control']) !!}</td>
                            <td>{!!  Form::text('discontinued', $product->discontinued, ['class'=> 'form-control']) !!}</td>
                            <td>
                                {!! Form::hidden('id',$product->id) !!}
                                {!!  Form::submit('Update', ['class'=> 'btn btn-primary form-control']) !!}
                            </td>
                            {!! Form::close() !!}
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $products->links() }}
            </div>
        </div>
    </div>
@stop



{!! Form::model($article, ['method' => 'PATCH', 'action' => ['ArticlesController@update', $article->id]]) !!}
{{--temporary--}}
{!! Form::hidden('user_id',1) !!}
<!-- Form Input -->
<div class="form-group">
    {!!  Form::label('title', 'Title:') !!}
    {!!  Form::text('title', null, ['class'=> 'form-control']) !!}
</div>
<div class="form-group">
    {!!  Form::label('body', 'Body:') !!}
    {!!  Form::textarea('body', null, ['class'=> 'form-control']) !!}
</div>
<div class="form-group">
    {!!  Form::label('published_at', 'Published_On:') !!}
    {!!  Form::input('date', 'published_at', date('Y-m-d'), ['class'=> 'form-control']) !!}
</div>
<div class="form-group">
    {!!  Form::submit($submitButton, ['class'=> 'btn btn-primary form-control']) !!}
</div>
{!! Form::close() !!}