@extends('welcome')

@section('content')
    @if (session('status'))
        <script>
            $( document ).ready(function() {
                $( "[value='{{ session('status') }}']").parent().parent().css( {"border": "3px solid #ff0000", "border-radius":"0.25rem"} );
            });
        </script>
{{--        <div class="alert alert-success">--}}
{{--            {{ session('status') }}--}}
{{--        </div>--}}
    @endif
    <div class="container-fluid">
        <h2>Dashboard</h2>
        <div class="row">
            <div class="col">
                <table class="table table-sm">
                    <thead>
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Password</th>
                        <th scope="col">Password confirmation</th>
                        <th scope="col">Role_id</th>
                        <th scope="col">Update</th>
                        <th scope="col">Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>id</td>
                        {!! Form::open(['method' => 'PUT', 'action' => ['DashboardController@addUser']]) !!}
                        <td>{!!  Form::text('name', '', ['class'=> 'form-control']) !!}</td>
                        <td>{!!  Form::text('email', '', ['class'=> 'form-control']) !!}</td>
                        <td>{!!  Form::text('password', '', ['class'=> 'form-control']) !!}</td>
                        <td>{!!  Form::text('password_confirmation', '', ['class'=> 'form-control']) !!}</td>
                        <td>role_id</td>
                        <td>
                            {!!  Form::submit('Add', ['class'=> 'btn btn-primary form-control']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                    @foreach($users as $user)
                        <tr>
                            <td class="id">{{ $user->id }}</td>
                            {!! Form::open(['method' => 'PATCH', 'action' => ['DashboardController@updateUser']]) !!}
                            <td>{!!  Form::text('name', $user->name, ['class'=> 'form-control']) !!}</td>
                            <td class="email">{!!  Form::text('email', $user->email, ['class'=> 'form-control']) !!}</td>
                            <td>{!!  Form::text('password', '', ['class'=> 'form-control']) !!}</td>
                            <td>{!!  Form::text('password_confirmation', '', ['class'=> 'form-control']) !!}</td>
                            <td>role_id</td>

                            <td>
                                {!! Form::hidden('id', $user->id) !!}
                                {!!  Form::submit('Update', ['class'=> 'btn btn-primary form-control']) !!}
                                {!! Form::close() !!}
                            </td>
                            <td>
                                {!! Form::open(['url' => 'admin/users']) !!}
                                {!! Form::hidden('id', $user->id) !!}
                                {!!  Form::submit('Delete', ['class'=> 'btn btn-primary form-control']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $users->links() }}
            </div>
        </div>
    </div>
@stop