<tr>
    <td>id</td>
    {!! Form::open(['method' => 'PUT', 'action' => ['DashboardController@addProduct']]) !!}
    <td>{!!  Form::text('name', '', ['class'=> 'form-control']) !!}</td>
    <td>{!!  Form::text('code', '', ['class'=> 'form-control']) !!}</td>
    <td>{!!  Form::text('description', '', ['class'=> 'form-control']) !!}</td>
    <td>{!!  Form::number('stock', '', ['class'=> 'form-control']) !!}</td>
    <td>{!!  Form::number('cost', '', ['class'=> 'form-control','step' => '0.01']) !!}</td>
    <td>published_at</td>
    {!! Form::hidden('published','0') !!}
    <td>{!!  Form::checkbox('published', '1', true, ['class'=> 'form-control']) !!}</td>
    <td>discontinued data</td>
    {!! Form::hidden('discontinued','') !!}
    <td>{!!  Form::checkbox('discontinued', 'yes', false, ['class'=> 'form-control']) !!}</td>
    <td>

        {!!  Form::submit('Add', ['class'=> 'btn btn-primary form-control']) !!}
        {!! Form::close() !!}
    </td>
</tr>