<tr>
    <td>{{ $product->id }}</td>
    {!! Form::open(['method' => 'PATCH', 'action' => ['DashboardController@updateProduct']]) !!}
    <td>{!!  Form::text('name', $product->name, ['class'=> 'form-control']) !!}</td>
    <td>{{ $product->code }}</td>
    {!! Form::hidden('code',$product->code) !!}
    <td>{!!  Form::text('description', $product->description, ['class'=> 'form-control']) !!}</td>
    <td>{!!  Form::number('stock', $product->stock, ['class'=> 'form-control']) !!}</td>
    <td>{!!  Form::number('cost', $product->cost, ['class'=> 'form-control','step' => '0.01']) !!}</td>
    <td>{{ $product->published_at }}</td>
    {!! Form::hidden('published','0') !!}
    <td>{!!  Form::checkbox('published', '1', $product->published, ['class'=> 'form-control']) !!}</td>
    <td>{{ $product->discontinued }}</td>
    {!! Form::hidden('discontinued','') !!}
    <td>{!!  Form::checkbox('discontinued', 'yes', false, ['class'=> 'form-control']) !!}</td>
    <td>
        {!! Form::hidden('id',$product->id) !!}
        {!!  Form::submit('Update', ['class'=> 'btn btn-primary form-control']) !!}
        {!! Form::close() !!}
    </td>
    <td>
        {!! Form::open(['url' => 'admin']) !!}
        {!! Form::hidden('id',$product->id) !!}
        {!!  Form::submit('Delete', ['class'=> 'btn btn-primary form-control']) !!}
        {!! Form::close() !!}
    </td>
</tr>